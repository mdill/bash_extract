# BASH extraction tool

## Purpose

This BASH script is designed to take an argument and "unzip" it based on what
type of compression was used to zip it.  This is just a quick way to take care
of something that I didn't want to deal with every time I needed to unzip a file
because I am lazy.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/bash_extract.git

## Placement

It is suggested that this script be placed in `/usr/local/bin/` for simplicity
and ease-of-use.  Once this is done, it can be made executable by:

    $ chmod +x /usr/local/bin/extract

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/bash_extract/src/21886d615d1208fbe60645c06f14bda23b0045ff/LICENSE.txt?at=master) file for
details.

